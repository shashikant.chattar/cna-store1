package com.wipro;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.SKU;
import com.wipro.SKURepository;
import com.wipro.SKUPayload;

import java.util.*; 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class InventoryController {
	
	@Autowired
	SKURepository repository;

	@GetMapping("/skus")
	@CrossOrigin(origins = "*")
	public List<SKU> getSKUlist(HttpServletResponse Httpresponse) throws IOException {
		List<SKU> skuList = new ArrayList<SKU>();
		for(SKU sku:repository.findAll()) {
			skuList.add(sku);
		}
		return skuList;		
	}
	
	@GetMapping("/skus/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> getSKUById(@PathVariable Integer id,HttpServletResponse Httpresponse) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		SKU sku = new SKU();
		
		sku = repository.findById(id);

		if(sku == null) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("not found",headers,HttpStatus.NOT_FOUND);
		}
		headers.set("Content-Type", "application/json");
		return new ResponseEntity<>(sku,headers,HttpStatus.OK);	
	}
	
	@PostMapping("/skus")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> newSKU (@RequestBody SKUPayload skuPayload)  throws IOException {	
		HttpHeaders headers = new HttpHeaders();
		if(RequestUtils.emptyOrMissing(skuPayload.getDescription()) 
				|| RequestUtils.emptyOrMissing(skuPayload.getName())
				|| skuPayload.getProductId() == null
				|| skuPayload.getPrice() == null
				|| skuPayload.getCount() == null) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("productId is mandatory",headers,HttpStatus.BAD_REQUEST);
		}
		else {
			SKU sku = new SKU();
			sku.setCount(skuPayload.getCount());
			sku.setDescription(skuPayload.getDescription());
			sku.setName(skuPayload.getName());
			sku.setPrice(skuPayload.getPrice());
			sku.setProductId(skuPayload.getProductId());
			SKU success = repository.save(sku);
			if(success==null) {
				headers.set("Content-Type", "text/plain");
				return new ResponseEntity<>("productId is mandatory",headers,HttpStatus.BAD_REQUEST);
			}
			else {
				headers.set("Content-Type", "application/json");
				return new ResponseEntity<>(success,headers,HttpStatus.CREATED);
			}		
		}
		
	}

	@PutMapping("/skus/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> updateSKU (@PathVariable Integer id, @RequestBody SKUPayload skuPayload)  throws IOException {
		HttpHeaders headers = new HttpHeaders();
		SKU skuToUpdate = new SKU();
		skuToUpdate = repository.findById(id);
		
		if(skuToUpdate == null) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("not found",headers,HttpStatus.NOT_FOUND);
		}
		
		skuToUpdate.setCount(skuPayload.getCount());
		skuToUpdate.setDescription(skuPayload.getDescription());
		skuToUpdate.setId(id);
		skuToUpdate.setName(skuPayload.getName());
		skuToUpdate.setPrice(skuPayload.getPrice());
		skuToUpdate.setProductId(skuPayload.getProductId());
	    repository.save(skuToUpdate);
	    headers.set("Content-Type", "application/json");
	    return new ResponseEntity<>(skuToUpdate,headers,HttpStatus.OK);
	    
	}
	
	@DeleteMapping("/skus/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> deleteSKUById(@PathVariable Integer id,HttpServletRequest request, HttpServletResponse Httpresponse) throws IOException {
		HttpHeaders headers = new HttpHeaders();		
		SKU sku = new SKU();
		sku = repository.findById(id);
		
		if(sku == null) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("not found",headers,HttpStatus.NOT_FOUND);
		}
		else {
			repository.delete(sku);
			headers.set("Content-Type", "application/json");
		    return new ResponseEntity<>(sku,headers,HttpStatus.OK);
		}				
	}
}
