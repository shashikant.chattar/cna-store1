package com.wipro;
import java.util.List;
import com.wipro.SKU;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SKURepository extends CrudRepository<SKU, Long> {
	
	SKU findById(Integer id);
	
}
