package com.wipro;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestUtils {

    private static Set<Class<?>> getWrapperTypes()
    {
        Set<Class<?>> ret = new HashSet<Class<?>>();
        ret.add(Boolean.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(BigInteger.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        ret.add(String.class);
        return ret;
    }
    
    private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();
    
    public static boolean isWrapperType(Class<?> clazz) {
        return WRAPPER_TYPES.contains(clazz);
    }
    
	public static void printFields( Object obj ) {
		Logger LOGGER = LoggerFactory.getLogger(obj.getClass());
		Map<String,String> fieldMap = new LinkedHashMap<String,String>();
		try {
			String qualifiedClassName = obj.getClass().getName();
			
			Method[] allPublicMethods = Class.forName(qualifiedClassName).getMethods();
			Field[] allFields = Class.forName(qualifiedClassName).getDeclaredFields();
			for ( Field field : allFields ) {
				String annotationParamName = field.getName();				
				if ( field.isAnnotationPresent(XmlElement.class) ) {
					annotationParamName = field.getAnnotation(XmlElement.class).name();
					if ( "##default".equals(annotationParamName)) annotationParamName = field.getName();
				} 
		        Method getter = null;
		        String value = null;
		        Object valueObj = null;
		        try {
		        	getter = getMatchingGetterMethod( allPublicMethods, field.getName() );

		            if ( getter == null ) continue;
		            
		            try {
		            	valueObj = getter.invoke(obj);
		            	if ( isWrapperType(valueObj.getClass()) ) {
		            		value = valueObj.toString();
		            	} else printFields(valueObj); // RECURSION
		            	//value = getter.invoke(obj).toString();
		            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}

		            if ( value != null ) {
		            	fieldMap.put(annotationParamName, value);
		            }
		        } catch (Exception ex) {}
			}
		} catch (SecurityException | ClassNotFoundException e) {}

		if ( fieldMap.isEmpty() ) return;
		int longestLength = 0;
		for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
			if ( entry.getKey().length() > longestLength ) longestLength = entry.getKey().length();
		}

		longestLength += 5;
		StringBuffer buffer = new StringBuffer(longestLength);
		for ( int i=0; i < longestLength; ++i ) buffer.append(".");

		String templateString = buffer.toString();
		for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
			String line = entry.getKey() + templateString.substring(entry.getKey().length(), templateString.length() );
			LOGGER.debug( line + entry.getValue() );
		}
	}
	
	private static Method getMatchingGetterMethod( Method[] allPublicMethods, String fieldName ) {
		for ( Method _method : allPublicMethods ) {

			String methodName = _method.getName();
			int parameterCount = _method.getParameterCount();
			if ( methodName.equals("getClass") == false 
					&& methodName.startsWith("get") 
					&& parameterCount == 0
					&& void.class.equals(_method.getReturnType() ) == false )
			{
				String strippedName = methodName.substring(3, methodName.length() );
				if ( strippedName.equalsIgnoreCase(fieldName )) return _method;
			}
		}
		
		return null;
	}
	
	public static boolean emptyOrMissing( String inputField ) {
		if ( inputField == null || inputField.trim().isEmpty()) return true;
		return false;
	}
	
	public static String normalizeBirthDate( String birthDate) {
		if ( RequestUtils.emptyOrMissing(birthDate)) return "";
		String normalizedBirthDate = birthDate;
		if ( birthDate.length() == 10 ) {

			String year = birthDate.substring(0, 4);
			String month = birthDate.substring(5, 7);
			String day = birthDate.substring(8);

			normalizedBirthDate = year + month + day;
		}
		return normalizedBirthDate;
	}
	
	public static String getAgeFromBirthdate(String birthDate) {
		String normalizedBirthDate = normalizeBirthDate( birthDate );
		if (normalizedBirthDate == null || normalizedBirthDate.length() != 8)
			return "";
		String age = "";
		try {
			int year = Integer.parseInt(normalizedBirthDate.substring(0, 4));
			int month = Integer.parseInt(normalizedBirthDate.substring(4, 6));
			int day = Integer.parseInt(normalizedBirthDate.substring(6));
			
			LocalDate tempLocalDate = LocalDate.of(year, month, day);
			LocalDate currentDate = LocalDate.now();
			Period years = Period.between(tempLocalDate, currentDate);
			age = String.valueOf(years.getYears());

		} catch (Exception e) {
			return "";
		}

		return age;
	}
	
	public static boolean isNumeric(String strNum) {
		if ( strNum == null || strNum.trim().isEmpty() ) return false;
	    try {
	        Double.parseDouble(strNum.trim());
	        return true;
	    }catch (NumberFormatException e) {
	        return false;
	    }
	    
	}
	
	public static String getJSON(SKU sku) {
		String strJSON = "";
		strJSON = strJSON + "{ \"id\":"+sku.getId()+",\n\"productId\":"+sku.getProductId()+",\r\n" + 
				"\"name\": \""+sku.getName() + "\",\n"+
				  "\"description\":\""+sku.getDescription()+ 
				  "\",\n\"price\":"+sku.getPrice() +
				  ",\n\"count\":" + sku.getCount() +
				  "\n }";
		return strJSON;
	}
}
