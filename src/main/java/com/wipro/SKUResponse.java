package com.wipro;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.wipro.SKU;

public class SKUResponse {
	
	private Integer Code;
	private String Description;
	private ArrayList<SKU> Links;
	
	public Integer getCode() {
		return Code;
	}
	public void setCode(Integer code) {
		Code = code;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public ArrayList<SKU> getLinks() {
		return Links;
	}
	public void setLinks(ArrayList<SKU> links) {
		Links = links;
	}


}
